<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("login");
});

Route::get("/login",[AuthController::class, "login"])->name('login');
Route::post("/login", [AuthController::class, "attempt"])->name("attempt");

Route::middleware("auth")->group(function(){

    Route::prefix("dashboard")->name("dashboard.")->group(function(){

        Route::get("/", [DashboardController::class,"index"])->name("index");
    });

    Route::prefix("report")->name("report.")->group(function(){
        Route::get("/timesheet", [ReportController::class,"timesheet"])->name("timesheet");
    });

    Route::get("logout", [AuthController::class,"logout"])->name("logout");
});