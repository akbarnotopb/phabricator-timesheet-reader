@extends('layouts.index')

@section('additional_style')
    <style type="text/css">
        .badge{
            margin: 0px;
        }
    </style>
@endsection

@section('sidebar')
    @include('admin.template.sidebar')
@endsection

@section('content')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 15px;padding-bottom: 15px">
            <h4><span class="text-semibold">Dashboard</span> - Statistik</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{ url('dashboard.index') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
        </ul>

        <ul class="breadcrumb-elements">
            <li><a href="#"><i class="icon-calendar52 position-left"></i><b>{{date('d-M-Y')}}</b></a></li>
        </ul>
    </div>
</div>

<div class="content" id="app">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body panel-body-accent">
                <h6 class="content-group text-semibold">
                    Selamat Datang {{ \Auth::User()->nama }}!
                    <small class="display-block"></small>
                </h6>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection

@section('additional_script')

    <script type="text/javascript">
        let vm = new Vue({
            el:"#app",
            data:{
                loading : false,
                data : {},
            },
            created:function(){
                $('#sidebar_admin_dashboard').addClass('active')
            },
            methods:{
                
            },
            computed:{

            },
            watch : {
                
            },
        });
    </script>
@endsection