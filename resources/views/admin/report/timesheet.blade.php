@extends('layouts.index')

@section('additional_style')
    <style type="text/css">
        .badge{
            margin: 0px;
        }

        .datatable-scroll-wrap .dropdown.dropup.open .dropdown-menu {
            display: flex;
        }
        .datatable-scroll-wrap .dropdown.dropup.open .dropdown-menu li a {
            display: flex;
        }
        .badge{
            margin: 0px;
        }
        @import url('//cdn.datatables.net/1.10.2/css/jquery.dataTables.css');
        td.details-control {
            background: url('http://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('http://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
    </style>
@endsection

@section('sidebar')
    @include('admin.template.sidebar')
@endsection

@section('content')

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title padding-15">
            <h4><a href="{{ route("dashboard.index") }}" style="color: black"><i class="icon-arrow-left52 position-left"></i></a>
            <span class="text-semibold">Report</span> - Timesheet</h4>
        </div>

        <div class="heading-elements">
            <ul class="breadcrumb" style="margin-top: 5px">
                <li><a href="#"><i class="icon-city position-left active"></i> Timesheet</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content" id="app">
    <div class="row">
        @include('layouts.notifikasi')
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Daftar Timesheet</h5> 
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="col-lg-12 row" style="margin-bottom:25px; padding:0;" >
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-3 col-xs-12">
                            <input type="text" name="daterange" id="daterangepicker" class="form-control" value="" />
                        </div>
                    </div>
                    <table class="table table-bordered table-hover table-striped table-sm" id="tbl_data">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Username Collab</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Durasi (Jam)</th>
                                <th>Project</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach ($data as $key => $item)
                              <tr>
                                  <td>{{ $key+1 }} </td>
                                  <td>{{ $item->user }}</td>
                                  <td>{{ $item->started_at}} </td>
                                  <td>{{ $item->ended_at }} </td>
                                  <td>{{ $item->duration }} </td>
                                  <td>{{ $item->project}} </td>
                                  <td>{{ $item->title}} </td>
                              </tr>
                          @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection

@section('additional_script')
    <script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('limitless/assets/js/pages/datatables_extension_buttons_html5.js') }}"></script>


    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script type="text/javascript">
        $(document).ready(function(){
            $("#tbl_data").DataTable({
                dom: 'Blfrtip',
                "aLengthMenu": [[5, 10, 15, 20, 25, 50, -1], [5, 10, 15, 20, 25, 50, 'All']],
                "iDisplayLength": 20,
                buttons: {            
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            className: 'btn btn-default',
                            exportOptions: {
                                columns: [ 0, ':visible' ]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            className: 'btn btn-default',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                            className: 'btn bg-blue btn-icon'
                        }
                    ]
                }
            });

            $("#sidebar_admin_timesheet_container").click()
            $('#sidebar_admin_timesheet').addClass('active')

            $('#daterangepicker').daterangepicker({
                "timePicker": true,
                "timePicker24Hour": true,
                "timePickerSeconds": true,
                startDate:"{{ $start->format('d/m/Y H:i:s') }}",
                endDate:"{{ $end->format('d/m/Y H:i:s') }}",
                locale:{
                    format:"D/M/YYYY H:m:s"
                },
                opens: 'left'
            }, function(start, end, label) {
                str = "{{ route('report.timesheet') }}?start="+start.format("DD/MM/YYYY H:mm:ss")+"&end="+end.format("DD/MM/YYYY H:mm:ss")
                window.location.href=str
            });
        })
    </script>
@endsection