<div class="col-lg-12">
    @if (Session::has('success'))
        <div class="alert alert-success alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
            <h4><i class="icon-checkmark"></i> Success!</h4>
            {!! Session::get('success') !!}
        </div>
    @endif
    @if (Session::has('danger'))
        <div class="alert alert-danger alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
            <h4><i class="icon-checkmark"></i> Success!</h4>
            {!! Session::get('danger') !!}
        </div>
    @endif
    @if (Session::has('failed'))
        <div class="alert alert-danger alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
            <h4><i class="icon-close2"></i> Gagal !</h4>
            {!! Session::get('failed') !!}
        </div>
    @endif
</div>