<head>
    @include('layouts.metadata')
    <title> Timesheet Hexavara</title>

    @include('layouts.style')

    @yield('additional_style')
</head>