<!DOCTYPE html>
<html>
	@include('layouts.header')
<body>
	@include('layouts.top_navbar')
	<div class="page-container">
        <div class="page-content">
        	@include('layouts.navbar')
        	<div class="content-wrapper">
        		@yield('content')
        		<div id="modal_general" class="modal inmodal fade" role="dialog" aria-hidden="true">
        		</div>
        	</div>
        </div>
    </div>
	@include('layouts.script')
	@yield('additional_script')
</body>
</html>