<script type="text/javascript" src="{{ asset('js/vue-devel.js') }}"></script>

<!-- Core JS files -->
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/loaders/pace.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/core/libraries/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/core/libraries/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('limitless/assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/pages/datatables_advanced.js') }}"></script>
<script src="{{ asset('js/selectize.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.pulsate.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/buttons/spin.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/plugins/buttons/ladda.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('limitless/assets/js/pages/components_buttons.js') }}"></script>
<!-- /theme JS files -->

<script>
    $(function() {
        jQuery('.pulse').pulsate({
            color: "#bf1c56",
            speed: 500,
            reach: 20,
            glow: true,
        });
    });
</script>