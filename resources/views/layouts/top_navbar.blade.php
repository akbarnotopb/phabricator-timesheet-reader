<div class="navbar navbar-inverse bg-primary-color">
    <div class="navbar-header bg-primary-color">
        <a class="navbar-brand" href="{{ url('#') }}" style="padding: 5px 20px">
            <img src="##" style="min-height: 30px; min-width: 100px;" alt="">
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                {{--
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bell3 pulse"></i>
                        <span class="visible-xs-inline-block position-right">Notifikasi</span>
                        <span class="badge bg-brown">1</span>
                    </a>
                
                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            <b>1</b> Pemberitahuan Baru
                            <ul class="icons-list">
                                <li><a href="#"><i class="icon-sync"></i></a></li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body width-350">
                            <li class="media">
                                <a href="#" title="Klik untuk lihat selengkapnya">
                                    <div class="media-left">
                                        <span class="btn border-success text-success btn-flat btn-rounded btn-icon btn-xs"><i class="icon-cash3"></i></span>
                                        <span class="label bg-success" style="margin-top: 5px">New</span>
                                    </div>
                                    <div class="media-body">
                                        <font style="color: black">Pemberitahuan Text</font>
                                        <div class="media-annotation">Kemarin</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                --}}

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('limitless/assets/images/user.png') }}" alt="">
                        <span>{{\Auth::User()->nama}}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-check"></i> My Profile</a></li>
                        <li><a href="#"><i class="icon-key"></i> Ganti Password</a></li>
                        <li class="divider"></li>
                        <li><a href="{{Route('logout')}}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>