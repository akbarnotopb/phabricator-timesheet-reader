<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/core.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/components.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('limitless/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('sweetalert/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('css/selectize.css') }}" rel="stylesheet">
<link href="{{ asset('css/selectize.bootstrap3.css') }}" rel="stylesheet">
<link href="{{ asset('css/loader.css') }}" rel="stylesheet">
<!-- <link rel="stylesheet" href="{{ asset('simplepicker/dist/simplepicker.css') }}"> -->
<!-- /global stylesheets -->

<style type="text/css">
    /* Header */
    .bg-primary-color{
        background-color: #59ba29;
    }
    .blink_me {
        animation: blinker 1s linear infinite;
    }

    .padding-15 {
    	padding: 15px;
    }

    .padding-10 {
        padding: 10px;
    }

    .control-label{
        font-weight: 500;
    }

    .vertical-center {
		margin: 0;
		position: absolute;
		top: 50%;
		-ms-transform: translateY(-50%);
		transform: translateY(-50%);
	}

    .hr-margin{
        margin-top: 7px;
        margin-bottom: 7px;
    }

    .no-padding{
        padding: 0px;
    }

    .btn-outline {
        background-color: transparent;
        color: inherit;
        transition: all .5s;
    }

    .btn-primary.btn-outline {
        color: #428bca;
    }

    .btn-success.btn-outline {
        color: #5cb85c;
    }

    .btn-info.btn-outline {
        color: #5bc0de;
    }

    .btn-warning.btn-outline {
        color: #f0ad4e;
    }

    .btn-danger.btn-outline {
        color: #d9534f;
    }

    .btn-primary.btn-outline:hover,
    .btn-success.btn-outline:hover,
    .btn-info.btn-outline:hover,
    .btn-warning.btn-outline:hover,
    .btn-danger.btn-outline:hover {
        color: #fff;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }
</style>