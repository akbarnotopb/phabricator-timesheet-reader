<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left">
                        <img src="{{ \Auth::User()->getFotoProfil() }}" class="img-circle img-sm" alt="">
                    </a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{\Auth::User()->nama}}</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> {{\Auth::User()->role}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    @yield('sidebar')

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>