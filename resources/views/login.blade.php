<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body class="login-container">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse bg-primary-color">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('#') }}" style="padding: 5px 20px">
                <img src="#" style="min-height: 30px; min-width: 100px;" alt="">
            </a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            {{-- <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul> 

            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-link2 position-left"></i>
                            <b>Menu</b>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#" target="_blank"><i class="icon-office"></i> Example</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                        </ul>
                    </li>
                </ul>
            </div>--}}
        </div>
    </div>
    <!-- /main navbar -->

    <div class="page-container">
        <div class="page-content">
            <div class="content-wrapper">
                <div class="content">
                    <form action="{{ Route('attempt') }}" method="POST">
                        @csrf
                        <div class="panel panel-body login-form text-center">
                            <div class="border-orange-800 text-orange-800">
                                <img src="#" style="max-height: 100px;max-width: 100px">
                            </div>
                            <h5 class="content-group">Timesheet Hexavara<small class="display-block">Welcome</small></h5>
                            @if (Session::has('alert-fail'))
                                <div class="alert alert-danger">
                                    <i class="fa fa-info-circle"></i> {{ Session::get('alert-fail') }}
                                </div>
                            @endif

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" name="password" placeholder="Kata sandi" minlength="6" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-primary-color btn-ladda btn-ladda-progress btn-block" data-style="expand-left" data-spinner-size="20">
                                    <span class="ladda-label">Masuk <i class="icon-circle-right2 position-right"></i></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.script')
</body>
</html>
