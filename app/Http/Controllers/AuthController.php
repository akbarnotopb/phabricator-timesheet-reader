<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(request $request){
        return view("login");
    }
    
    public function attempt(Request $request){
        try{
            $creds = $request->only(["email","password"]);
            
            if(Auth::attempt($creds)){
                switch (Auth::User()->role) {
                    case 'admin':
                        return redirect()->route('dashboard.index');
                        break;
                    default:
                        Auth::logout();
                        return redirect()->route("login");
                        break;
                }
            }else{
                return redirect()->back()->with(["alert-failed"=>"Email / Password salah!"])->withInput();
            }
        }
        catch(\Exception $e){
            return redirect()->back()->with(["alert-failed"=>"Pesan : ".$e->getMessage()]);
        }
    }

    public function logout(Request $request){
        auth()->logout();
        return redirect()->route("login");
    }
}
