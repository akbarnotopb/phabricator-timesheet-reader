<?php

namespace App\Http\Controllers;

use App\Models\Phrequent;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function timesheet(Request $request){

        $start = Carbon::now();
        $end   = Carbon::now();
        $month = $start->month;

        if($start->gt(Carbon::create($start->year,$month, 26,0,0,1))){
            // if 26 27 28 ... , use this month as start and end on the next month
            $start = Carbon::create($start->year,$month, 26,0,0,1);
            $end   = Carbon::create($start->year,$month, 25, 23,59,59);
        }else{
            // if new month i.e., 1 2 3 4 , end this month 
            $end   = Carbon::create($start->year,$month, 25, 23,59,59);
            // and use last month as start
            if($month == 1){
                $month = 12;
            }else{
                $month = $month - 1;
            }
            $start = Carbon::create($start->year,$month, 26,0,0,1);
        }

        if($request->has("start") && $request->has("end")){
            try{
                $r_start    = Carbon::createFromFormat("d/m/Y H:i:s", $request->start);
                $r_end      = Carbon::createFromFormat("d/m/Y H:i:s", $request->end);

                $start      = $r_start;
                $end        = $r_end;
            }catch(Exception $e){
                // use old start & end
            }
        }



        $data = Phrequent::join('phabricator_maniphest.maniphest_task', 'maniphest_task.phid', '=', 'phrequent_usertime.objectPHID')
        ->leftjoin('phabricator_user.user', 'phrequent_usertime.userPHID', '=', 'user.phid')
        ->leftjoin('phabricator_project.edge', 'edge.dst', '=', 'maniphest_task.phid')
        ->join('phabricator_project.project', 'project.phid', '=', 'edge.src')
        ->selectRaw('user.userName AS `user`, TIME_TO_SEC(
            TIMEDIFF(
              FROM_UNIXTIME(phrequent_usertime.dateEnded), 
              FROM_UNIXTIME(phrequent_usertime.dateStarted)
            )
          )/3600 AS duration,
          FROM_UNIXTIME(phrequent_usertime.dateStarted) AS started_at,
          FROM_UNIXTIME(phrequent_usertime.dateEnded) AS ended_at,
          phabricator_maniphest.maniphest_task.title as title,
          phabricator_project.project.name as project
          ')
        ->whereRaw("FROM_UNIXTIME(phrequent_usertime.dateEnded) BETWEEN '".$start->format("Y-m-d H:i:s")."' AND '".$end->format("Y-m-d H:i:s")."'")
        ->orderBy('user')
        ->get();

        return view("admin.report.timesheet", compact("start", "end", "data"));
    }

    
}
