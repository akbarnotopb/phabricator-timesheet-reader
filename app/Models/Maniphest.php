<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maniphest extends Model
{
    use HasFactory;

    protected $table        = "maniphest_task";
    protected $connection   = "phabricator_maniphest";
}
