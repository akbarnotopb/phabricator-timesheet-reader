<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phrequent extends Model
{
    use HasFactory;

    protected $table        = "phrequent_usertime";

    protected $connection   = "phabricator_phrequent";

}
