<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PUser extends Model
{
    use HasFactory;

    protected $table      = "user";
    protected $connection = "phabricator_user";
}
